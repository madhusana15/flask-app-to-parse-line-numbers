from flask import Flask,render_template
app = Flask(__name__)
file="file1.txt"
N=""
@app.route('/hello/<file>/<int:N>')
def read_file1(file,N):
    
    try:
        f=open(file,'r',encoding="latin-1")
    except Exception as e:
        f=open('file1.txt','r',encoding="latin-1")
    if N:
        try: 
            data=f.readlines()
            for index, l in enumerate(data):
                if N==index:
                    data="Line: {}".format(l.strip())
        
        except Exception as e:
            data=f.readlines()
  
    else:
        data=f.readlines()
    print(data)
    print(type(data))
    return render_template('hello.html', text = data)


if __name__ == '__main__':
   app.run(debug = True)


